from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from mpesa_configs import broker_params, logger
from datetime import datetime

import base64
import MySQLdb
import MySQLdb.cursors
import logging
import eventlet
import hashlib
import requests
from requests.auth import HTTPBasicAuth

logging.basicConfig(level=logging.INFO)
#requests_log = logging.getLogger('requests.packages.urllib3').setLevel(logging.DEBUG)

#requests_log.propagate = True

class MPesa():
         
    def get_access_token(self, consumer_key, consumer_secret):
	req = None
        try:
            #r = requests.get(broker_params['auth_url'], auth=HTTPBasicAuth(consumer_key, consumer_secret))
            req = requests.get(broker_params['auth_url'], auth=HTTPBasicAuth(consumer_key, consumer_secret))
            logger.info("Response [%s]" % req.text)
        except requests.exceptions.RequestException as e:
            logger.error(e)
            raise
        return req
    def encrypt_initator_password (self, init_password) :
        """
            Encrypt init_password using RSASSA-PKCS #1 v1.5.
            Base 64 Encode String
            Args:
                init_password (str) : Unencrypted Initiator Password
            Returns:
                encrypted_password (str)
        """
        pubkey = open(broker_params['certificate']).read()
        key = RSA.importKey(pubkey)
        cipher = PKCS1_v1_5.new(key)
        return base64.b64encode(cipher.encrypt(init_password))

    def send_mpesa_request(self, initiator_params, access_token, init_encr_pass, amount, msisdn):
        response = None
        headers = { "Authorization": "Bearer %s" % access_token }
        request = {
          "InitiatorName": initiator_params['initiator'],
          #"SecurityCredential":init_encr_pass,
          "SecurityCredential":"ahCfjgqXV5VRaZiYLzkAuBnDTzOOp0G9TDfU5qZ3KdCpzOFmOAXN4qGEz2akxH9in5kgrck4m4aKtC6JLu566SM27gf/MegInUU8wPW1dujsoqSiUeBpa83mcNpbL0vv9QCeVmBf/o4hVOfaJZ9MVfTXkv0dpykZYBRSoFzDQ9yJk9V4j7exz3Y62XQutqOOPAT9OtzLJ1EPldaWuO2PCgKtZPG3LeQTWStAhcnE9akETU4l9DlKgCsHN1IqEiLpAXCMh2rd0WNR5AO7Pk4zK6nn8EM7VqD/TGQzydhKTSqSPXTbIHI7A2mIiCzsYEXWdCv0OWvT0g0mJXqYgwiH1A==",
          "CommandID": initiator_params['command_id'],
          "Amount": str(amount).split('.')[0],
          #"Amount": str(amount),
          "PartyA": str(initiator_params['shortcode']),
          "PartyB": str(msisdn),
          "Remarks": "Withdrawal by " + str(msisdn),
          "QueueTimeOutURL": initiator_params['timeout'],
          "ResultURL": initiator_params['result'],
          "Occasion": "Jamhuri"
        }
	logger.info("M-Pesa Request [%s]" % request)
        try :
            response = requests.post(broker_params['payment_request'], json = request, headers=headers)
            logger.info("M-Pesa Response [%s]" % response.text)
        except requests.exceptions.RequestException as err:    
            logger.error("Esception %s" % err)
            raise
        return response


    def generic_api_request(self, initiator_params, transaction_id, msisdn, amount):
        """
           Make M-Pesa request
           Args:
               service_params (dict) :
               transaction_id (str) : Merchant Transaction ID
               amount (float) :
               msisdn (str): Mobile number of the customer receiving M-Pesa funds
           Returns:
               result (dict):
           Raises:
               Exception
        """
        
        init_encr_pass = self.encrypt_initator_password(initiator_params['init_password'])
        response = {'OriginatorConversationID':"ERROR", 'ConversationID':"ERROR", 'ResponseDescription':"ERROR"} 
        try:
            r = self.get_access_token(initiator_params['consumer_key'], initiator_params['consumer_secret'])
            if r.status_code == requests.codes.ok:
                token = r.json()
                res = self.send_mpesa_request(initiator_params, token['access_token'], init_encr_pass, amount, msisdn)
                if res.status_code == requests.codes.ok:
		    response = res.json()
                else:
		    result = res.json()
		    response = {'OriginatorConversationID':result['requestId'], 'ConversationID':result['requestId'], 'ResponseDescription':result['errorMessage']}
        except Exception, e:
            logger.error(e)
            raise
        return response
