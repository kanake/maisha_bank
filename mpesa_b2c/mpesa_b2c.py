#!/usr/bin/python -u

from MPesa import MPesa
from MySQL import MySQL
from mpesa_configs import logger, config, mysql_params, initiator_params
from functools import partial

import time
import datetime
import logging
import ConfigParser
import os
import signal
import MySQLdb
import MySQLdb.cursors
import sys
import eventlet

#from tendo import singleton

#si = singleton.SingleInstance()

eventlet.monkey_patch()

is_sending = 0
is_waiting_to_die = 0

mysql = MySQL()

#to be accessed in process_mpesa
service_params = None

def signal_handler(signum, frame):
    global is_sending, is_waiting_to_die
    logger.info('SIGNAL:\tSIGTERM')
    if is_sending:
        is_waiting_to_die = 1
        logger.info('SENDING...\t WAIT!')
        return
    logger.info('DIE')
    exit(0)

def init_service_params(service):
    """ Parameters specific to the service
    """
    global service_params
    sections = config.sections()
    try:
        index = sections.index(service)
        name = config.get(sections[index],'name')
        service_params = {'name':name}
    except Exception, e:
        raise

def service_init(args):
    """ Defines how the application is started.
    args = terminal arguments used to run the service """
    app_list = config.sections()
    reserved_sections = ['mpesa', 'mysql', 'logger', 'service']

    #Invalid Arguments or Invalid Params
    if len(args) < 2:
        logger.error("USAGE: python %s <app_name>" % args[0])
        exit(0)
    elif sys.argv[1] not in app_list:
        logger.error('Invalid Args: App %s must be in the config file' % args[1])
        exit(0)
    elif args[1] in reserved_sections:
        logger.error('Invalid Args: %s is a reserved section' % args[1])
        exit(0)
    else:
        try:
            init_service_params(args[1])
            logger.info('%s: %s Started! Name: %s'
                    % (datetime.datetime.now(), args[1], service_params['name']))
        except Exception, e:
            logger.error('Invalid Params in the configuration file. %s' % e)
            exit(0)

def _fetch_from_queue(connection=None):
    """ Retrieve from mpesa_disburse transactions
    """

    sql = """SELECT id, transaction_id, client_id, msisdn, amount, created_at
                FROM mpesa_disburse GROUP BY msisdn LIMIT 100
          """
    params = ()
    try:
        trx_list = mysql.retrieve_all_data_params(connection, sql, params)
        return trx_list
    except Exception, e:
        logger.error(e)
        raise

def _check_duplicates(trx, connection):
    """ Retrieve from mpesa_disburse transactions
    """

    sql = """SELECT msisdn, amount
                FROM mpesa_disburse_log WHERE msisdn = %s AND AMOUNT = %s
                AND DATE(date_created) = DATE(NOW())
          """
    params = (trx['msisdn'], trx['amount'])
    try:
        trx_list = mysql.retrieve_all_data_params(connection, sql, params)
        return trx_list
    except Exception, e:
        logger.error(e)
        raise

def _delete_transaction(id, connection):
    del_sql = 'DELETE FROM mpesa_disburse WHERE id = %s '
    params = (int(id),)
    try:
        mysql.execute_query(connection, del_sql, params)
        return True
    except Exception, e:
        logger.error('_delete_transaction: %s' % e)
        raise


def _push_to_processed(trx, mpesa_transaction_id, orig_trx_id, mpesa_resp_desc, connection):
    insert_sql = """INSERT INTO mpesa_disburse_complete
                        (transaction_id, client_id, msisdn, amount,
                        mpesa_transaction_id, orig_trx_id, mpesa_resp_desc,
                        created_at, updated_at)
                        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, NOW())
                 """
    params = (trx['transaction_id'], trx['client_id'], trx['msisdn'], trx['amount'],
                  mpesa_transaction_id, orig_trx_id, mpesa_resp_desc, trx['created_at'])
    try:
        mysql.execute_query(connection, insert_sql, params)
        return True
    except Exception, e:
        logger.error(e)
        raise

def _create_log(trx, connection):
    insert_sql = """INSERT INTO mpesa_disburse_log
                        (transaction_id, msisdn, amount,
                        date_created)
                        VALUES(%s, %s, %s, NOW())
                 """
    params = (trx['transaction_id'], trx['msisdn'], trx['amount'])
    try:
        mysql.execute_query(connection, insert_sql, params)
        return True
    except Exception, e:
        logger.error(e)
        raise


def dispatch(connection):
    """ Pops transactions from mpesa_disburse
    """
    global is_sending, is_waiting_to_die
    trx_list = _fetch_from_queue(connection)
    if not trx_list:
        return
   
    try:
        pool_size = 10
        is_sending = True
        pool = eventlet.GreenPool(size=pool_size)
        for response in pool.imap(partial(process_mpesa, connection), trx_list):
            logger.info('RESP: OriginatorConversationID:%s, ConversationID:%s, ResponseDescription:%s' %\
                    (response['OriginatorConversationID'], response['ConversationID'], response['ResponseDescription']))
        is_sending = False
        if is_waiting_to_die:
            logger.info('NOT SENDING:\tDIE.')
            exit(0)
    except Exception, e:
        logger.error(e)
        is_sending = 0
        if is_waiting_to_die:
            logger.info('NOT SENDING:\tDIE.')
            exit(0)


def process_mpesa(connection, trx):

    
    response = {'OriginatorConversationID':None, 'ConversationID':None, 'ResponseDescription':None}
    if not len(trx):
        return response
    #connection = None
    mpesa = MPesa()
    try:
        _delete_transaction(trx['id'], connection)

        logger.info('REQ: [CLIENTID]:[%s][TRANSACTIONID]:[%s][MSISDN]:[%s][AMOUNT]:[%s]'\
                %(trx['client_id'], trx['transaction_id'], trx['msisdn'], trx['amount'] ))
        #trx_list = _check_duplicates(trx, connection)
        #if not len(trx_list):
        response = mpesa.generic_api_request(initiator_params[trx['client_id']], trx['transaction_id'],\
                             trx['msisdn'], trx['amount'])
        logger.info('RESP:[CONVERSATIONID]:[%s][ORIGINAL CONV ID]:[%s][RESPONSEDESC]:[%s]'\
            %(response['ConversationID'], response['OriginatorConversationID'], response['ResponseDescription']))
        conversation_id = response['ConversationID']
        orig_id = response['OriginatorConversationID']
        resp_desc = response['ResponseDescription']
        _push_to_processed(trx, conversation_id, orig_id, resp_desc, connection)
            #_create_log(trx, connection)
        #else:
            #_push_to_processed(trx, "DUPLICATE", "DUPLICATE", "DUPLICATE", connection)
    except Exception, e:
        logger.error(e)
        _delete_transaction(trx['id'], connection)
        _push_to_processed(trx, "ERROR", "ERROR", e, connection)
        #_create_log(trx, connection)
    connection.commit()
    return response    


def create_connection():
    """ Creates a connection and returns the connection """
    try:
        connection = MySQLdb.connect(host=mysql_params['host'],\
                user=mysql_params['user'], passwd=mysql_params['passwd'],\
                db=mysql_params['db'], cursorclass=MySQLdb.cursors.DictCursor)
    except MySQLdb.Error, e:
        logger.error(e)
        raise
    return connection

if __name__=='__main__':
    service_init(sys.argv)
    signal.signal(signal.SIGTERM, signal_handler)
    while True:
        try:
            connection = create_connection()
            dispatch(connection)
        except MySQLdb.Error, e:
            logger.error("MySQL Error: %s" % e)
        finally:
            try:
                connection.close()
            except Exception ,e:
                logger.error("MySQL Error closing connection : %s" % e)
        time.sleep(1)
