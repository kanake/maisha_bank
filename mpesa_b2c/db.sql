CREATE TABLE `mpesa_disburse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) NOT NULL,
  `client_id` varchar(20) DEFAULT NULL,
  `msisdn` varchar(12) NOT NULL,
  `amount` double(7,2) NOT NULL,
  `mpesa_transaction_id` varchar(50) DEFAULT NULL,
  `mpesa_resp_desc` varchar(100) DEFAULT NULL,
  `node` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `date_processed` datetime DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



 CREATE TABLE `mpesa_disburse_complete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) NOT NULL,
  `client_id` varchar(20) DEFAULT NULL,
  `msisdn` varchar(12) NOT NULL,
  `amount` double(7,2) NOT NULL,
  `mpesa_transaction_id` varchar(50) DEFAULT NULL,
  `orig_trx_id` varchar(50) DEFAULT NULL,
  `mpesa_resp_desc` varchar(100) DEFAULT NULL,
  `reversal_status` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_processed` datetime DEFAULT NULL,
  `date_odified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



 CREATE TABLE `mpesa_disburse_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) NOT NULL,
  `client_id` varchar(20) DEFAULT NULL,
  `msisdn` varchar(12) NOT NULL,
  `amount` double(7,2) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;