import logging
import ConfigParser

CONFIG_FILE = r'/usr/local/lib/maisha/mpesa_b2c/mpesa.conf'
config = ConfigParser.ConfigParser()
config.read(CONFIG_FILE)
#logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("MPESA_B2C")
logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
#hdlr = logging.FileHandler(config.get("logger", "log_file"))
hdlr = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)

host = config.get("mysql", "host")
port = config.get("mysql", "port")
user = config.get("mysql", "user")
passwd = config.get("mysql", "password")
db = config.get("mysql", "database")
connection_timeout = config.get("mysql", "connection_timeout")
mysql_params = {
        'host':host,
        'port':port,
        'user':user,
        'passwd':passwd,
        'db':db,
        'connection_timeout':connection_timeout
        }

auth_url = config.get("mpesa", "auth_url")
payment_request = config.get("mpesa", "payment_request")
certificate = config.get("mpesa", "certificate")

sp_id = config.get("service", "sp_id")
sp_password = config.get("service", "sp_password")
service_id = config.get("service", "service_id")

broker_params = {
    'auth_url':auth_url,
    'payment_request':payment_request,
    'certificate':certificate,
}


initiator_params = {
    'maisha': {
        'initiator' : config.get("maisha", "initiator"),
        'init_password' : config.get("maisha", "init_password"),
        'shortcode' : config.get("maisha", "shortcode"),
        'command_id' : config.get("maisha", "command_id"),   
        'timeout' : config.get("maisha", "timeout"), 
        'result' :  config.get("maisha", "result"),
        'consumer_key' : config.get("maisha", "consumer_key"), 
        'consumer_secret' :  config.get("maisha", "consumer_secret"),      
    }
}
