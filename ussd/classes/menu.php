<?php

$menu_registration = array(
    "1" => array("title" => "Welcome to Mfanisi.\r\nBy activating you agree to terms and conditions, view terms and conditions at www.maishabank.com.\n\r1.Register", "minValue" => 1, "maxValue" => 1, "datatype" => 'int', 'input' => 1, "type" => 'CON'),
    "1*1" => array("title" => "Please enter your ID number\r\n\r\n0. Main Menu\r\n99.Back", "minValue" => 6, "maxValue" => 10, "datatype" => 'string', "type" => 'CON', 'input' => 0),
    "1*1*0" => array("title" => "Please enter your name as per your ID number\r\n\r\n0. Main Menu\r\n99.Back", "minValue" => 6, "maxValue" => 50, "datatype" => 'string', "type" => 'CON', 'input' => 0),
    "1*1*0*0" => array("title" => "functionCall", 'function' => 'confirm_registration', "minValue" => 1, "maxValue" => 2,  "type" => 'CON', 'input' => 1, 'datatype' => 'int'),
  
    "1*1*0*0*1" => array("title" => "functionCall", "type" => 'END', 'function' => 'register_member', 'level' => 4),
    "1*1*0*0*2" => array("title" => "Dear customer, your have cancelled your registration.\r\nPlease contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank for assistance", "type" => 'END',),
); 

$menu_first_time_login = array(
    "1" => array("title" => "functionCall", "function" => "welcome_inactive", "minValue" => 4, "maxValue" => 4, "datatype" => 'string', 'input' => 0, "type" => 'CON'),
    "1*0" => array("title" => "functionCall", "function" => "user_otp", "minValue" => 4, "maxValue" => 4, "datatype" => 'string', "type" => 'CON', 'input' => 0),
    "1*0*0" => array("title" => "Confirm new PIN\r\n\r\n0. Main Menu\r\n99.Back", "minValue" => 4, "maxValue" => 4, "datatype" => 'confirm_pin', "type" => 'CON', 'input' => 0, 'datatype' => 'confirm_pin'),
    "1*0*0*0" => array("title" => "functionCall", "type" => 'END', 'function' => 'activate_user', 'level' => 4),
);

$menu_account_suspended = array(
    "1" => array("title" => "You have exhausted the trials", "type" => 'END', 'level' => 0, "minValue" => 4, "maxValue" => 6, 'datatype' =>""),
);

$menu_pin_blocked = array(
    "1" => array("title" => "Dear customer your Mobile Banking account has been suspended.\r\nPlease contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank to activate your account", "type" => 'END',  "minValue" => 0, "maxValue" => 0, 'datatype' =>"int"),
);

$menu_invalid_user_status = array(
    "1" => array("title" => "Dear customer, we cannot process your request at the moment. \r\nPlease contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank.", "type" => 'END', "minValue" => 0, "maxValue" => 6, 'datatype' =>"int"),
);

/**
 * Continuing user menu
 */

$menu_registered = array(
    "1" => array("title" => "functionCall", "minValue" => 1, "maxValue" => 6, 'input' => 0, "type" => 'int', "type" => 'CON', 'datatype' => 'string', 'function' => 'registered'),
    //My Account
    "1*0" => array("title" => "functionCall", 'function' => 'authenticate_user', 'input' => 1, "minValue" => 1, "maxValue" => 8, 'datatype' => 'int', "type" => 'CON'),

    "1*0*1" => array("title" => "Enter amount\r\n\r\n0. Back\r\n00. Home", "minValue" => 10, "maxValue" => 70000, "type" => 'CON', 'input' => 0, 'datatype' => 'amount'),
    "1*0*1*0" => array("title" => "functionCall", 'function' => 'deposit_cash', "minValue" => 0, "maxValue" => 0,  "type" => 'END', 'datatype' => 'string'),   
    "1*0*2" => array("title" => "M-fanisi to M-pesa.\r\nEnter Amount\r\n\r\n0. Back\r\n00. Home", "minValue" => 50, "maxValue" => 30000, "type" => 'CON', 'input' => 0, 'datatype' => 'amount'),
    "1*0*2*0" => array("title" => "Withdraw from M-fanisi\r\nEnter PIN\r\n\r\n0. Back\r\n00. Home", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*2*0*0" => array("title" => "functionCall", 'function' => 'send_to_mpesa', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),

    "1*0*3" => array("title" => "Loans\r\n1. Request Loan\r\n2. Repay Loan\r\n3. Loan Balance\r\n4. Check loan limit\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 4, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),
    "1*0*3*1" => array("title" => "functionCall", 'function' => 'Loan_amount_eligible', "type" => 'CON', "minValue" => 1, "maxValue" => 2, 'datatype' => 'int', 'input' => 1),
    "1*0*3*1*1" => array("title" => "functionCall", 'function' => 'Loan_amount', "minValue" => 500, "maxValue" => 70000, 'input' => 0, 'datatype' => 'loan_limit'),
    "1*0*3*1*2" => array("title" => "Dear customer, your have cancelled this request.\r\nPlease contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank for assistance", "type" => 'END'),
    "1*0*3*1*1*0" => array("title" => "functionCall", 'function' => 'validate_amount_eligible', "type" => 'CON', "minValue" => 1, "maxValue" => 1, 'datatype' => 'int'),
    "1*0*3*1*1*0*0" => array("title" => "Loan\r\nEnter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*3*1*1*0*0*0" => array("title" => "functionCall", 'function' => 'confirm_loan_request', "minValue" => 1, "maxValue" => 2,  "type" => 'CON', 'input' => 1, 'datatype' => 'int'),
    "1*0*3*1*1*0*0*0*1" => array("title" => "functionCall", 'function' => 'request_mloan', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),
    "1*0*3*1*1*0*0*0*2" => array("title" => "Dear customer, your have cancelled this request.\r\nPlease contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank for assistance", "type" => 'END'),



    "1*0*3*2" => array("title" => "Repay Loan from\r\n1. M-Pesa\r\n2. M-Fanisi\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),

    "1*0*3*2*1" => array("title" => "functionCall", "function" => 'repay_loan_balance', "minValue" => 1, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    "1*0*3*2*1*0" => array("title" => "functionCall", 'function' => 'mpesa_repay_loan', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),


    "1*0*3*2*2" => array("title" => "functionCall", "function" => 'repay_loan_balance', "minValue" => 1, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    "1*0*3*2*2*0" => array("title" => "Enter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*3*2*2*0*0" => array("title" => "functionCall", 'function' => 'mfanisi_repay_loan', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),

    


    "1*0*3*3" =>  array("title" => "Enter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*3*3*0" => array("title" => "functionCall", 'function' => 'loan_balance', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),

    "1*0*3*4" =>  array("title" => "Check loan limit\r\nEnter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*3*4*0" => array("title" => "functionCall", 'function' => 'loan_limit', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),


    "1*0*4" => array("title" => "Savings\r\n1. My Savings\r\n2. Fixed Deposits\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),
    "1*0*4*1" => array("title" => "My savings\r\n1. Save\r\n2. Withdraw\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),

    "1*0*4*1*1" => array("title" => "Save from\r\n1. M-Fanisi\r\n2. M-Pesa\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),

    "1*0*4*1*1*1" => array("title" => "Enter Amount [Ksh 1-70000]", "type" => 'CON', "minValue" => 1, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    "1*0*4*1*1*1*0" => array("title" => "Enter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*4*1*1*1*0*0" => array("title" => "functionCall", 'function' => 'mfanisi_save', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),

    "1*0*4*1*1*2" => array("title" => "Enter Amount [Ksh 1-70000]", "type" => 'CON', "minValue" => 1, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    "1*0*4*1*1*2*0" => array("title" => "functionCall", 'function' => 'mpesa_save', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),


    //"1*0*4*1*2" => array("title" => "Withdraw from\r\n1. M-Fanisi\r\n2. M-Pesa\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),

    "1*0*4*1*2" => array("title" => "Enter Amount [Ksh 1-70000]", "type" => 'CON', "minValue" => 1, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    "1*0*4*1*2*0" => array("title" => "Enter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*4*1*2*0*0" => array("title" => "functionCall", 'function' => 'mfanisi_withdraw', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),

    //"1*0*4*1*2" => array("title" => "Enter Amount [Ksh 500-70000]", "type" => 'CON', "minValue" => 500, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    //"1*0*4*1*2*0" => array("title" => "functionCall", 'function' => 'mpesa_withdraw', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),


    "1*0*4*2" => array("title" => "Fixed Deposits\r\n1. Create Fixed deposit\r\n2. Withdraw\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),
    "1*0*4*2*1" => array("title" => "Create Fixed deposit\r\n1. M-Fanisi\r\n2. M-Pesa\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 2, "type" => 'CON', 'input' => 0, 'datatype' => 'int'),
    "1*0*4*2*1*0" => array("title" => "From M-fanisi Enter period(months) (1 - 12 Months)\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 12, "type" => 'CON', 'input' => 0, 'datatype' => 'int'),

    "1*0*4*2*1*0*0" => array("title" => "Enter Amount [Ksh 500-70000]", "type" => 'CON', "minValue" => 100, "maxValue" => 70000, 'input' => 0, 'datatype' => 'amount'),
    "1*0*4*2*1*0*0*0" => array("title" => "functionCall", 'function' => 'confirm_fixed_deposit', "minValue" => 1, "maxValue" => 2,  "type" => 'CON', 'input' => 1, 'datatype' => 'int'),
    "1*0*4*2*1*0*0*0*1" => array("title" => "functionCall", 'function' => 'fixed_deposit', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),
    "1*0*4*2*1*0*0*0*2" => array("title" => "Dear customer, your have cancelled this request.\r\nPlease contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank for assistance", "type" => 'END'),
    "1*0*4*2*2" => array("title" => "functionCall", 'function' => 'fd_receipts', "minValue" => 0, "maxValue" => 1, 'datatype' => 'no_check'),

    "1*0*4*2*2*0" => array("title" => "functionCall", 'function' => 'confirm_fd_receipts', "minValue" => 0, "maxValue" => 0, 'datatype' => 'no_check'),

    "1*0*4*2*2*0*0" => array("title" => "Enter PIN", "minValue" => 4, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'string'),
    "1*0*4*2*2*0*0*0" => array("title" => "functionCall", 'function' => 'fd_withdraw', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),   


    "1*0*5" => array("title" => "My Account\r\n1. Check Balance\r\n2. Mini statement\r\n3. Change PIN\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 3, "type" => 'CON', 'input' => 1, 'datatype' => 'int'),

    "1*0*5*1" => array("title" => "Check Balance\r\n1. M-Fanisi Account\r\n2. Loan Balance\r\n3. Locked Savings\r\n4. Fixed Deposits\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 4, "type" => 'CON', 'input' => 0, 'datatype' => 'int'),
    "1*0*5*1*0" => array("title" => "functionCall", 'function' => 'check_balance', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'), 

    "1*0*5*2" => array("title" => "Mini Statement\r\n1. M-Fanisi Account\r\n2. Locked Savings\r\n3. Fixed Deposits\r\n\r\n0. Back\r\n00. Home", "minValue" => 1, "maxValue" => 3, "type" => 'CON', 'input' => 0, 'datatype' => 'int'),
    "1*0*5*2*0" => array("title" => "functionCall", 'function' => 'mini_statement', "minValue" => 0, "maxValue" => 0, 'datatype' => 'string'),
    "1*0*5*3" => array("title" => "Enter old PIN", "minValue" => 4, "maxValue" => 4, "datatype" => 'string', "type" => 'CON', 'input' => 0),
    "1*0*5*3*0" => array("title" => "functionCall", 'function' => 'authenticate_user_pin', "minValue" => 1, "maxValue" => 4, "datatype" => 'string', "type" => 'CON', 'input' => 0),
    "1*0*5*3*0*0" => array("title" => "Confirm new PIN\r\n\r\n0. Main Menu\r\n99.Back", "minValue" => 4, "maxValue" => 4, "datatype" => 'confirm_pin', "type" => 'CON', 'input' => 0, 'datatype' => 'confirm_pin'),
    "1*0*5*3*0*0*0" => array("title" => "functionCall", "type" => 'END', 'function' => 'change_pin', 'level' => 4),
);

