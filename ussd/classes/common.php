<?php
#define("LOG_FILE", "/var/log/sdp_myinfo/sdp_GitaNorwich.log");

require_once 'messages.php';
require_once 'Bcrypt.php';



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Processor
 *
 * @author KANAKE
 */
class common {
    function __construct() {
        $this->configs = parse_ini_file("config.ini", true);
    }

    function curl_post($url, $post_params, $headers) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->configs['urls']['endpoint'] . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",

            CURLOPT_POSTFIELDS => json_encode($post_params),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return $response;
        }
    }


    function check_reg_status($msisdn) {
        $post_params = array("msisdn" => $msisdn);
        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
        );
        
        $response  = $this->curl_post("check_if_exist", $post_params, $headers);
        
        if ($response) {
            return json_decode($response, true);
        } else {
            echo "END Dear Customer. We are unable to process your request at the moment. Please try later";
            die();
        }
    }




    function register_user($msisdn, $name, $id_number) {
        global $messages;

        $names = explode(" ", $name);

        $post_params = array(
          "first_name" => isset($names[0])?$names[0]:"",
          "other_name" => isset($names[1])?$names[1]:"",
          "surname" => isset($names[2])?$names[2]:"-",
          "msisdn" => $msisdn,
          "id_number" => $id_number,
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
        );

        $response  = $this->curl_post("register", $post_params, $headers);
       
        if ($response) {
            $resp = json_decode($response, true);
            if ($resp['responseCode'] == "0") {
                $message =  "END " . $messages['REG_PENDING'];
            } else {
                $message = $messages['ERROR'];
            }
            
        } else {
            $message = $messages['ERROR'];
        }
        return $message;
    }

    function login($msisdn, $pin) {
        global $messages;

        $post_params = array(
          "msisdn" => $msisdn,
          "password" => $pin
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
        );

        $response  = $this->curl_post("login", $post_params, $headers);
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }


    function change_pin($msisdn, $current_pin, $new_pin, $token) {
        global $messages;
        $post_params = array(
          "current_pin" => $current_pin,
          "new_pin" => $new_pin,
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("change_pin", $post_params, $headers);
        if ($response) {
            $resp = json_decode($response, true);

            return $resp;
        } else {
           echo  $messages['ERROR'];
            die();
        }
        
    } 


    function deposit($amount, $channel, $token) {
        global $messages;
        $post_params = array(
          "amount" => $amount,
          "method" => $channel,
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("deposit", $post_params, $headers);

        if ($response) {
            $resp = json_decode($response, true);

            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
       
    } 
     
    function withdraw($amount, $token) {
        global $messages;
        $post_params = array(
          "amount" => $amount
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("withdraw", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);

            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
        
    } 


    function loan_limit($token) {
        global $messages;
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("loan/get/limit", $post_params, $headers);
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
        
    } 


    function request_mloan ($amount, $period, $token) {
        $post_params = array(
          "amount" => $amount,
          "period" => $period
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("loan/request", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function loan_balance ($token) {
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("loan/get/balance", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function loan_details ($token) {
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("loan/get/details", $post_params, $headers);

        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }


    function loan_calculate ($amount, $period, $token) {
        $post_params = array(
            "principle" => $amount,
            "period" => $period
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("loan/calculate", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function repay_loan ($amount, $channel, $token) {
        $post_params = array(
          "amount" => $amount,
          "account" => $channel
        );
        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("loan/repay", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }


    function savings_save ($amount, $channel, $token) {
        $post_params = array(
          "amount" => $amount,
          "account" => $channel
        );
        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("savings/save", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function savings_withd ($amount, $channel, $token) {
        $post_params = array(
          "amount" => $amount,
          "account" => $channel
        );
        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("savings/withdraw", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function fixed_deposit ($amount, $channel, $period,  $token) {
        $post_params = array(
          "amount" => $amount,
          "account" => $channel,
          "period" => $period,
        );
        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("fixed_savings/deposit", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function fixed_receipts ($token) {
        $post_params = array(
        );
        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("fixed_savings/get/receipts", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }


    function fixed_withdrawal ($receipt, $channel, $token) {
        $post_params = array(
          "receipt" => $receipt,
          "account" => $channel,
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("fixed_savings/withdraw", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function fixed_interest_rate ($period, $token) {
        $post_params = array(
            "period" => $period
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("fixed_savings/interest_rate", $post_params, $headers);
        
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }

    function savings_balance ($token) {
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("savings/get/balance", $post_params, $headers);

        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }   

    function check_ministatement ($token) {
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("check_balance", $post_params, $headers);
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }   
    
       function check_balance ($token) {
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );

        $response  = $this->curl_post("check_balance", $post_params, $headers);
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }
 
    function statement ($product, $token) {
        $post_params = array(
        );

        $headers = array (
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Bearer " . $token,
        );
        $url = "";
        switch ($product) {
            case "st";
            $url = "savings/get/statement";
            break;
            case "fd":
            $url = "fixed_savings/get/statement";
            break;
            case "ts":
            $url = "statement";
            break;
        }
        $response  = $this->curl_post($url, $post_params, $headers);
        if ($response) {
            $resp = json_decode($response, true);
            return $resp;
        } else {
            echo  $messages['ERROR'];
            die();
        }
    }
    
    function validateAmount ($value) {
        return preg_match("/^[0-9]+(?:\.[0-9]{1,2})?$/", $value);
    }
    
    function to_money($val,$symbol='Ksh ',$r=2)
    {
        $n = $val; 
        $c = is_float($n) ? 1 : number_format($n,$r);
        $d = '.';
        $t = ',';
        $sign = ($n < 0) ? '-' : '';
        $i = $n=number_format(abs($n),$r); 
        $j = (($j = strlen($i)) > 3) ? $j % 2 : 0;

       return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;

    }
    
  
}
