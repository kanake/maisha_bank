<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$messages = array(
    "CONFIRM_REGISTRATION" => "You will be registered as\r\n%s, %s\r\n1.Confirm\r\n2.Cancel\r\n\r\n0. Back\r\n00. Home",
    "REG_PENDING" => "Please wait as we process your registration request.",
    "MAIN_MENU" => "M-fanisi Menu\r\n1.Deposit to M-fanisi\r\n2.Withdraw from M-fanisi\r\n3.Loan\r\n4.Savings\r\n5.My account\r\n\r\n000. Log Out",
    "ACCOUNT_LOCKED" => "You have been locked out please contact our customer care through 07XXXXXX",
    "ACTIVATE_PIN" =>  "Enter your activation PIN",
    "NEW_PIN" => "Enter new PIN",
    "INVALID_OTP" => "M-fanisi\r\nWrong activation Key\r\nTry again",
    "PIN" =>  "Dear %s, Welcome to M-Fanisi Platform\r\nPlease enter your Pin to continue",
    "INVALID_PIN" => "Wrong PIN\r\nTry again",
    "MLOAN_CONFIRM" => "Confirm  Principle Amount: Kes %s\r\nInterest Kes :%s\r\nRepayment period: %s month\r\n1. Confirm\r\n2. Cancel\r\n\r\n0. Back\r\n00. Home",
    "LOAN_REPAYMENT_PERIOD" => "Select loan repayment period\r\n1. 1 month\r\n\r\n0. Back\r\n00. Home",
    "AMOUNT_ELIGIBLE" => "Request loan You are eligible to borrow a maximum amount of Ksh. %s for a max period of 1 month\r\n1. Proceed\r\n2. Cancel\r\n\r\n0. Back\r\n00. Home",
    "AMOUNT_ELIGIBLE_ERROR" => "Dear customer, amount entered is beyond your limit. You qualify for Ksh. %s. Please enter the correct amount",
    "CONFIRM_FIXED" => "You are about to fix Kshs %s for a period of %s Months, you will earn an interest of %s %s p.a\r\n1. Proceed\r\n2. Cancel\r\n\r\n0. Back\r\n00. Home",
    "END_CON" => "\r\n\r\n0. Back\r\n00. Home",
    "FDWD_CONFIRM" => "If you withdraw %s before maturity, you will forfeit the accrued interest\r\n1. Proceed\r\n2. Cancel\r\n\r\n0. Back\r\n00. Home",
    "REPAY_LOAN" => "Your loan balance is Ksh. %s. Enter the MLoan Amount to Repay [Ksh 1-70000]",
    "ERROR" => "END Dear Customer. We are unable to process your request at the moment. Please contact 020 222 0648 | 0736-028-982 | 0792-002-300 or visit Maisha Microfinance Bank for assistance",
    "LOAN_BALANCE" => "Dear Customer, your Loan balance is Ksh. %s due on %s. Please repay on time to qualify for a higher loan amount\r\n\r\n0. Back\r\n00. Home",
    "LOAN_LIMIT" => "Dear %s, You Qualify for M-Fanisi Loan of Ksh %s. Dial *281# to apply\r\n\r\n0. Back\r\n00. Home",
    "BALANCE" => "Dear Customer, your %s Balance is Kshs. %s.",
    "LOG_OUT" => "You have been logged out of your session. To login again dial *218#",
    "PIN_CHANGE" => "Dear %s your pin has been changed. Please login with the new PIN",
    "EXISTING_LOAN" => "Dear Customer, we cannot complete your request because you have an outstanding loan balance of Ksh %s. Please clear to continue enjoying our services",
);
