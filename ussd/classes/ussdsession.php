<?php


class ussdsession {
    
    function __construct() {
        $this->configs = parse_ini_file("config.ini", true);
    }
    
    /**
     * method clearSessions
     * deletes from the session
     * */
    function clearSessions($msisdn) {
        if (apc_exists($msisdn)) {
            apc_delete($msisdn);
        }
    }

    /**
     * Method createSessions
     * creates a new session for the user
     * */
    function createSessions( $session_id, $msisdn, $menu,  $ussd_string) {
        
        apc_store($msisdn, $menu . "|" . $ussd_string, 600);
    }

   

    /**
     * Method updateSessions 
     * Updates the session string on each request
     * */
    public  function updateSessions($session_id, $msisdn, $menu, $ussd_string) {

        apc_store($msisdn, $menu . "|" . $ussd_string, 600);
    }

    /**
     * Method getSessionString 
     * Updates the session string on each request
     * */
    function getSessionString($session_id, $msisdn) {
        $sessionString = array();
        if (apc_exists($msisdn)) {
            $data = explode("|", apc_fetch($msisdn));
            $sessionString[0] = $data[0];
            $sessionString[1] = $data[1];
            
        }
        return $sessionString;

    }

    function _execute($sql, $params) {
        
        $username = $this->configs['database']['username'];
        $password = $this->configs['database']['password'];
    
        $database = $this->configs['database']['dbname'];
        $host = $this->configs['database']['host'];
        try {
            $pdo = new PDO("mysql:host=$host;dbname=$database", $username, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);
            $pdo = null;
            $stmt = null;
        } catch (PDOException $error) {
            error_log("\n". $error, 3, LOG_FILE);
        }
    }

    function _select($sql, $params) {
        $username = $this->configs['database']['username'];
        $password = $this->configs['database']['password'];
        $database = $this->configs['database']['dbname'];
        $host = $this->configs['database']['host'];
        try {
            $pdo = new PDO("mysql:host=$host;dbname=$database", $username, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $pdo->prepare($sql);
            $stmt->execute($params);
            $row = $stmt->fetchAll();
            $pdo = null;
            $stmt = null;
            return $row;
        } catch (PDOException $error) {
            error_log("\n". $error, 3, LOG_FILE);
        }
    }

}
