<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
define("LOG_FILE", "/var/log/sdp_mo/ussd.log");

ini_set("soap.wsdl_cache_enabled", 0);

date_default_timezone_set("Africa/Nairobi");

require_once("classes/menu.php");
require_once("classes/ussdsession.php");
require_once("classes/common.php");
require_once("classes/messages.php");

$msisdn = isset($_REQUEST['MSISDN']) ? $_REQUEST['MSISDN'] : "";
$sessionId = isset($_REQUEST['session_id']) ? $_REQUEST['session_id'] : "";
$ussdString = isset($_REQUEST['ussd_string']) ? $_REQUEST['ussd_string'] : "";

$common = new common($messages);
$session = new ussdsession();

/*$whitelist = array(254720794196, 254726354124, 254720794196, 254725337854, 254721458462, 254715618090, 254723773655, 254719742768, 254724670942, 254720839260, 254705126329, 254719476140, 254725337854,254715618090,254723773655,254719742768,254724670942,254720839260,254724776547,254720839260,254721135035,254725861978, 254719742768);

if (!in_array($msisdn, $whitelist)) {
   echo "END This service is not available";
   die();
}*/
$user_status = $common->check_reg_status($msisdn);
#apc_clear_cache();
/**if (apc_exists($session_id . "_" . $msisdn)) {
    $user_status = apc_fetch($session_id . "_" . $msisdn);
} else {
    $user_status = $common->check_reg_status($msisdn);
    apc_store($session_id. "_" . $msisdn, $user_status, 600);
}**/
if ($user_status['responseCode'] == 1 && $user_status['responseData']['state'] == 1) {
    $user_reg_status = false;
} else {
    $user_reg_status = true;
}

if ($ussdString == "" or $ussdString == "293") {
    //clear user sessions
    $session->clearSessions($msisdn);
    $menu = "1";
    $sessionString = "1";
    $session->createSessions($sessionId, $msisdn, $menu, $sessionString);
    if (!$user_reg_status) {
        $menu = $menu_registration["1"];
    } else {
        if ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 0) {
            $menu = $menu_registered["1"];
        } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 3) {
            $menu = $menu_first_time_login["1"];
        } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 4) {
            $menu = $menu_account_suspended["1"];
            $menu['title'] = $menu["title"];
        } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 5) {
            $menu = $menu_pin_blocked["1"];
            $menu['title'] = $menu["title"];
        } else {

            $menu = $menu_invalid_user_status["1"];
            $menu['title'] = $menu["title"];
        }
    }
    if ($menu["title"] == "functionCall") {
        $response = call_user_func($menu["function"], $menu, $msisdn, $sessionId, $menu, $sessionString, false);
    } else {
        $response = $menu["type"] . " " . $menu["title"];
    }
} else {

    //short cuts
    $input = array_pop(explode("*", $ussdString));
    $savedMenuArr = $session->getSessionString($sessionId, $msisdn);

    $currentSavedMenu = $savedMenuArr[0];
    $currentUssdString = $savedMenuArr[1];
    if ($input === "000") {
        $session->clearSessions($msisdn);
        $response = "END " . $messages['LOG_OUT'];
    } elseif ($input === "0") {
        if (substr_count($currentSavedMenu, "*") >= 1) {
            $currentSavedMenuArr = explode("*", $currentSavedMenu);
            $currentUssdStringArr = explode("*", $currentUssdString);
            array_pop($currentSavedMenuArr);
            array_pop($currentUssdStringArr);
            $currentSavedMenu = implode("*", $currentSavedMenuArr);
            $currentUssdString = implode("*", $currentUssdStringArr);
        }

        if (!$user_reg_status) {
            $menu = $menu_registration[$currentSavedMenu];
        } else {
            if ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 0) {
                $menu = $menu_registered[$currentSavedMenu];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 3) {
                $menu = $menu_first_time_login[$currentSavedMenu];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 4) {
                $menu = $menu_account_suspended["1"];
                $menu['title'] = $menu["title"];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 5) {
                $menu = $menu_pin_blocked["1"];
                $menu['title'] = $menu["title"];
            } else {
                $menu = $menu_invalid_user_status["1"];
                $menu['title'] = $menu["title"];
            }
        }

        if ($menu["title"] == "functionCall") {
            $response = call_user_func($menu["function"], $menu, $msisdn, $sessionId, $currentSavedMenu, $currentUssdString, false);
        } else {
            $response = $menu["type"] . " " . $menu["title"];
        }
        $session->updateSessions($sessionId, $msisdn, $currentSavedMenu, $currentUssdString);
    } elseif ($input === "00") {
        /**
         *  For Registered users leave the first two parts of the menu
         *  For the rest take them to 1
         */
        if ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 0) {

            $currentSavedMenuArr = explode("*", $currentSavedMenu);
            $currentUssdStringArr = explode("*", $currentUssdString);


            array_splice($currentSavedMenuArr, 2);
            array_splice($currentUssdStringArr, 2);
            $currentSavedMenu = implode("*", $currentSavedMenuArr);
            $currentUssdString = implode("*", $currentUssdStringArr);

            $sessionString = $currentUssdString;
            $session->updateSessions($sessionId, $msisdn, $currentSavedMenu, $currentUssdString);
        } else {
            $session->clearSessions($msisdn);
            $menu = "1";
            $sessionString = "1";
            $session->createSessions($sessionId, $msisdn, $menu, $sessionString);
        }


        if (!$user_reg_status) {
            $menu = $menu_registration["1"];
        } else {
           if ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 0) {
                $menu = $menu_registered[$currentSavedMenu];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 3) {
                $menu = $menu_first_time_login[$currentSavedMenu];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 4) {
                $menu = $menu_account_suspended["1"];
                $menu['title'] = $menu["title"];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 5) {
                $menu = $menu_pin_blocked["1"];
                $menu['title'] = $menu["title"];
            } else {
                $menu = $menu_invalid_user_status["1"];
                $menu['title'] = $menu["title"];
            }
        }
        if ($menu["title"] == "functionCall") {
            $response = call_user_func($menu["function"], $menu, $msisdn, $sessionId, $menu, $sessionString, false);
        } else {
            $response = $menu["type"] . " " . $menu["title"];
        }
    } else {
        if (!$user_reg_status) {
            $menu = $menu_registration[$currentSavedMenu];
        } else {
            if ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 0) {
                $menu = $menu_registered[$currentSavedMenu];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 3) {
                $menu = $menu_first_time_login[$currentSavedMenu];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 4) {
                $menu = $menu_account_suspended["1"];
                $menu['title'] = $menu["title"];
            } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 5) {
                $menu = $menu_pin_blocked["1"];
                $menu['title'] = $menu_pin_blocked["title"];
            } else {
                $menu = $menu_invalid_user_status["1"];
                $menu['title'] = $menu["title"];
            }
        }
        $type = $menu['datatype'];
        $min = $menu['minValue'];
        $max = $menu['maxValue'];
        $status = false;

        switch ($type) {
            case "string":
                if (strlen($input) >= $min && strlen($input) <= $max) {
                    $status = true;
                }
                break;
            case "int":
                if ($input >= $min && $input <= $max) {
                    $input = (int) $input;
                    $status = true;
                }
                break;
            case "amount":
                if ($common->validateAmount($input)) {
                    if ($input >= $min && $input <= $max) {
                        $status = true;
                    }
                }
                break;
            case "loan_limit":
                $max_amount = 500;
                if (apc_exists($sessionId . "_loan_limit")) {
                    $resp = apc_fetch($sessionId . "_loan_limit");
	            $max_amount = $resp['responseData']['limit'];
                }
                if ($input >= $min && $input <= $max_amount) {
                    $input = (int) $input;
                    $status = true;
                }
                break;

            case "msisdn":
                $mobile = $common->validateMsisdn(substr($input, -9));
                if ($mobile > 0) {
                    $input = "254" . substr($mobile, -9);
                    $status = true;
                }
                break;
            case "account":
                #further checks on account
                if (strlen($input) >= $min && strlen($input) <= $max)
                    $status = true;
                break;
            case "pin_validate":

                if ($common->validate_pin($msisdn, $input))
                    $status = true;
                break;
            case "confirm_pin":
                $arr = array_reverse(explode("*", $currentUssdString));
                if ($input == $arr[0])
                    $status = true;
                break;
            case "no_check":
                $status = true;
                break;
        }

        if ($status) {
            //check if there is function handling this request
            if ($menu['input'] > 0) {
                $currentSavedMenu .= "*" . $input;
            } else {
                $currentSavedMenu .= "*0";
            }
            $currentUssdString .= "*" . $input;

            if (!$user_reg_status) {
                $menu = $menu_registration[$currentSavedMenu];
            } else {
                if ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 0) {
                    $menu = $menu_registered[$currentSavedMenu];
                } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 3) {
                    $menu = $menu_first_time_login[$currentSavedMenu];
                } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 4) {
                    $menu = $menu_account_suspended["1"];
                    $menu['title'] = $menu["title"];
                } elseif ($user_status['responseCode'] == 0 && $user_status['responseData']['state'] == 5) {
                    $menu = $menu_pin_blocked["1"];
                    $menu['title'] = $menu_pin_blocked["title"];
                } else {
                    $menu = $menu_invalid_user_status["1"];
                    $menu['title'] = $menu["title"];
                }
            }

            $session->updateSessions($sessionId, $msisdn, $currentSavedMenu, $currentUssdString);
            //$response = $menu["type"] . "  " . $menu["title"];
            if ($menu["title"] == "functionCall") {
                $response = call_user_func($menu["function"], $menu, $msisdn, $sessionId, $currentSavedMenu, $currentUssdString, $status);
            } else {
                $response = $menu["type"] . " " . $menu["title"];
            }
        } else {
            if ($menu["title"] == "functionCall") {
                $response = call_user_func($menu["function"], $menu, $msisdn, $sessionId, $currentSavedMenu, $currentUssdString, $status);
            } else {
                $response = $menu["type"] . " " . $menu["title"];
            }
        }
    }
}



function confirm_registration ($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $common, $messages;
    $user_details = array_reverse(explode("*", $session_string));
    $message = "CON " . sprintf($messages['CONFIRM_REGISTRATION'], $user_details[0], $user_details[1]);
    return $message;
}


function register_member ($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $common, $messages;
    $user_details = array_reverse(explode("*", $session_string));

    $message = $common->register_user($msisdn, $user_details[1], $user_details[2]);
    if (apc_exists($session_id)) {
        apc_delete($session_id);
    }
    if (apc_exists($session_id . "_" . $msisdn)) {
        apc_delete($session_id . "_" . $msisdn);
    }
    return $message;
}

function welcome_inactive($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $common, $messages, $user_details;
 
    $message = "CON " . $messages['ACTIVATE_PIN'];
    
    return $message;
}



function user_otp($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {

    global $common, $messages, $user_details, $session;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];
    $login = $common->login($msisdn, $pin);

    if ($login['responseCode'] == 0) {
        if (apc_exists($session_id)) {
            apc_delete($session_id);
        }
        apc_store($session_id, $login, 600);
        $message = "CON " . $messages['NEW_PIN'];
    } else {
        $session->updateSessions($sessionId, $msisdn, "1", "1");
        $message = "CON " . sprintf($messages['INVALID_OTP'] . ' ' . $messages['RE_PIN'], '0');
    }
    return $message;
}

function activate_user($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $common, $messages, $user_details, $session;
    $details = array_reverse(explode("*", $session_string));

    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }   
    $change_pin = $common->change_pin($msisdn, $details[2], $details[0], $resp['responseData']['token']);

    if ($change_pin['responseCode'] == 0) {
        $login = $common->login($msisdn, $details[0]);
        if (apc_exists($session_id)) {
            apc_delete($session_id);
        }
        apc_store($session_id, $login, 600);
        $session->updateSessions($session_id, $msisdn, "1*0", "1*" . $details[1]);
        return "CON " . $messages['MAIN_MENU'];
    }
}

function authenticate_user_pin($menu, $msisdn,  $session_id, $current_menu, $session_string, $status) {
    global $common, $messages, $user_details, $session;

    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];
    $login = $common->login($msisdn, $pin);

    if ($login['responseCode'] == 0) {
        if (apc_exists($session_id)) {
            apc_delete($session_id);
        }
        apc_store($session_id, $login, 600);
        $message = "CON " . "Enter new pin";
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
 
        $message = "CON " . $messages['INVALID_PIN'];
    }
    return $message;
}


function change_pin($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $common, $messages, $user_status, $session;
    $details = array_reverse(explode("*", $session_string));
      
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    } 
 
    $change_pin = $common->change_pin($msisdn, $details[2], $details[0], $resp['responseData']['token']);

    if ($change_pin['responseCode'] == 0) {
        $login = $common->login($msisdn, $details[0]);
        if (apc_exists($session_id)) {
            apc_delete($session_id);
        }
        return "END " . sprintf($messages['PIN_CHANGE'], $user_status['responseData']['name']) ;
    }
}


function registered($menu, $msisdn,  $session_id, $current_menu, $session_string, $status) {
    global $common, $messages, $user_status;
    $message = "CON " . sprintf($messages['PIN'], $user_status['responseData']['name']) ;
    return $message;
}

function authenticate_user($menu, $msisdn,  $session_id, $current_menu, $session_string, $status) {
    global $common, $messages, $user_details, $session;

    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];
    $login = $common->login($msisdn, $pin);

    if ($login['responseCode'] == 0) {
        if (apc_exists($session_id)) {
            apc_delete($session_id);
        }
        apc_store($session_id, $login, 600);
        $message = "CON " . $messages['MAIN_MENU'];
    } else {
        $session->updateSessions($sessionId, $msisdn, "1", "1");
        $message = "CON " . $messages['INVALID_PIN'];
    }
    return $message;
}

function deposit_cash($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $dep = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  
    $resp = $common->deposit($dep[0], "MPESA", $resp['responseData']['token']);
    if ($resp['responseCode'] == 0) {
        $message = sprintf("CON Your request to send KSH %s to your M-Fanisi account is being processed you will receive a confirmation message shortly\r\n\r\n0. Back\r\n00. Home",$dep[0]) ;
    } else {
        $message = $messages['ERROR'];
    }
    
    return $message;
}

function send_to_mpesa($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;

    $details = array_reverse(explode("*", $session_string));

    $pin = $details[0];
    
    $login = $common->login($msisdn, $pin);
    
    if ($login['responseCode'] == 0) {
        $resp = $common->Withdraw($details[1], $login['responseData']['token']);
        if ($resp['responseCode'] == 0) {
            $message = "CON " . sprintf("Your request to withdraw Kshs. %s from M-Fanisi is being processed. Kindly wait for an SMS notification\r\n\r\n0. Back\r\n00. Home", $details[1]);
        } else {
            $message = $messages['ERROR'];
        }
        
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Withdraw from M-fanisi\r\nInvalid PIN\r\nEnter PIN\r\n\r\n0. Back\r\n00. Home";
    }
    return $message;
}


//remember to check if a member has a loan balance before displaying this
function Loan_amount_eligible($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
   

    global $messages, $common, $user_details;
    $details = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }

    $lb = $common->loan_balance($resp['responseData']['token']);
    if ($lb['responseCode'] == 0) {
        if ($lb['responseData']['balance'] <= 0) {
            $loan_limit = $common->loan_limit($resp['responseData']['token']);

            if (apc_exists($session_id . "_loan_limit")) {
                apc_delete($session_id . "_loan_limit");
            }  
            apc_store($session_id . "_loan_limit", $loan_limit, 600);
            if ($login['responseCode'] == 0) {
                $message = "CON " . sprintf($messages['AMOUNT_ELIGIBLE'], $loan_limit['responseData']['limit']);
            } else {
                $message = $messages['ERROR'];
            }
        } else {
            $message = "END " . sprintf($messages['EXISTING_LOAN'], $lb['responseData']['balance']);
        }
    } else {
        $message = $messages['ERROR'];
    }
    return $message;
}




function loan_amount($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $user_details;
    $max_amount = 0;
    if (apc_exists($session_id . "_loan_limit")) {
        $resp = apc_fetch($session_id . "_loan_limit");
        $max_amount = $resp['responseData']['limit'];
    }
    
    if ($status) {
        $message = sprintf("Enter amount up to borrow up to Ksh. %s\r\n\r\n0. Back\r\n00. Home", $max_amount);
    } else {
        $message = sprintf("Dear customer, amount entered is beyond your limit. You qualify for  Ksh. %s\r\n\r\n0. Back\r\n00. Home", $max_amount);
    }
    return "CON " . $message;
}






function validate_amount_eligible($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $user_details, $session;
    $details = array_reverse(explode("*", $session_string));
    $max_amount = 0;
    if (apc_exists($session_id . "_loan_limit")) {
        $resp = apc_fetch($session_id . "_loan_limit");
        $max_amount = $resp['responseData']['limit'];
    }

    if ($details[0] <= $max_amount) {
        $message = $messages['LOAN_REPAYMENT_PERIOD'];
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));

        $message = sprintf("Enter amount up to borrow up to %s\r\n\r\n0. Back\r\n00. Home",  $max_amount);
    }
    return "CON " . $message;
}



function confirm_loan_request($menu, $msisdn, $sessionId, $currentMenu, $sessionString, $status) {
    global $messages, $common, $session;
    $loan = array_reverse(explode("*", $sessionString));

    $pin = $loan[0];
    
    $login = $common->login($msisdn, $pin);
    if ($login['responseCode'] == 0) {
        $loan = $common->loan_calculate ($loan[2], "1", $login['responseData']['token']);
        if ($loan['responseCode'] == 0) {

            $prin = $loan['responseData']["principle"];
            $period = $loan['responseData']["period"];
            $rate = $loan['responseData']["rate"];
            $interest = $loan['responseData']["interest"];
            $message = "CON " . sprintf($messages['MLOAN_CONFIRM'], $prin, $interest, $period); 
        } else {
            $message = $messages['ERROR'];
        }
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Loan\r\nInvalid PIN\r\nEnter PIN\r\n\r\n0. Back\r\n00. Home";
    }
    return  $message;
}


function request_mloan($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $loan = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  
    $amount = $loan[3];
    $resp = $common->request_mloan($amount, "1", $resp['responseData']['token']);
    if ($resp['responseCode'] == 0) {
        $message = "CON " . "Thank You for your M-Fanisi Loan application, you will receive an SMS confirmation shortly\r\n\r\n0. Back\r\n00. Home";
    } else {
        $message = $messages['ERROR'];
    }
    
    return $message;
}

function repay_loan_balance($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  

    $lb = $common->loan_balance($resp['responseData']['token']);
    
    if ($lb['responseCode'] == 0) {
        if (apc_exists($session_id . "_loan_balance")) {
            apc_delete($session_id . "_loan_balance");
        } 
        apc_store($session_id . "_loan_balance", $lb, 600);   
        $amount = $lb['responseData']['balance'];
        $message = "CON " . sprintf($messages['REPAY_LOAN'], $amount);
    } else {
        $message = "END You current do not have any pending loans";
    }
    
    return $message;
}



function mfanisi_repay_loan($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];

    $login = $common->login($msisdn, $pin);
    if ($login['responseCode'] == 0) {
        $resp = $common->repay_loan ($details[1], "MFANISI", $login['responseData']['token']);
        if ($resp['responseCode'] == 0) {
            $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
        } else {
            $message = $messages['ERROR'];
        }
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);

        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Invalid PIN. Enter PIN"; 
    }
    return $message;
}

function mpesa_repay_loan($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $details = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  
    $resp = $common->repay_loan ($details[0], "MPESA", $resp['responseData']['token']);
    if ($resp['responseCode'] == 0) {
        $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
    } else {
        $message = $messages['ERROR'];
    }

    
    return $message;
}

function loan_balance($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];

    $login = $common->login($msisdn, $pin);

    
    if ($login['responseCode'] == 0) {
        $lb = $common->loan_details($login['responseData']['token']);
        if ($lb['responseCode'] == 0) {
            $date = new DateTime($lb['responseData']['loan']['due_date']);
    
            $message = "CON " . sprintf($messages['LOAN_BALANCE'], $lb['responseData']['loan']['amount'], $date->format('d/m/Y') );
        } else {
            $message = $messages['ERROR'];
        }
        
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Invalid PIN. Enter PIN"; 
    }
    return $message;
}

function loan_limit($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session, $user_status;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];

    $login = $common->login($msisdn, $pin);

    
    if ($login['responseCode'] == 0) {
        $lt = $common->loan_limit($login['responseData']['token']);
        if ($lt['responseCode'] == 0) {
            $message = "CON " . sprintf($messages['LOAN_LIMIT'], $user_status['responseData']['name'] , $lt['responseData']['limit']);
        } else {
            $message = $messages['ERROR'];
        }
        
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Invalid PIN. Enter PIN"; 
    }
    return $message;
}



function mfanisi_save($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];

    $login = $common->login($msisdn, $pin);
    if ($login['responseCode'] == 0) {
        $resp = $common->savings_save ($details[1], "MFANISI", $login['responseData']['token']);
        if ($resp['responseCode'] == 0) {
            $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
        } else {
            $message = $messages['ERROR'];
        }
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Invalid PIN. Enter PIN"; 
    }
    return $message;
}

function mpesa_save($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $details = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  
    $resp = $common->savings_save ($details[0], "MPESA", $resp['responseData']['token']);
    if ($resp['responseCode'] == 0) {
        $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
    } else {
        $message = $messages['ERROR'];
    }
    return $message;
}



function mfanisi_withdraw($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];

    $login = $common->login($msisdn, $pin);
    if ($login['responseCode'] == 0) {
        $resp = $common->savings_withd ($details[1], "MFANISI", $login['responseData']['token']);
        
        if ($resp['responseCode'] == 0) {
            $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
        } else {
            $message = $messages['ERROR'];
        }
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Invalid PIN. Enter PIN"; 
    }
    return $message;
}

//change the interest rates  
function confirm_fixed_deposit($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $fixed = array_reverse(explode("*", $session_string));

    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  
    $fdi = $common->fixed_interest_rate ($fixed[1],  $resp['responseData']['token']);

    if ($fdi['responseCode'] == 0) {
        $message = "CON " . sprintf($messages['CONFIRM_FIXED'], $fixed[0], $fixed[1], $fdi['responseData']['interest_rate'], '%'); 
    } else {
         $message = $messages['ERROR'];
    }
    return $message;
}



function fixed_deposit($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  
    $channel = $details[3] == 1?"MFANISI":"MPESA";
    if ($resp['responseCode'] == 0) {
        $resp = $common->fixed_deposit ($details[1], $channel, $details[2], $resp['responseData']['token']);
        
        if ($resp['responseCode'] == 0) {
            $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
        } else {
            $message = $messages['ERROR'];
        }
    } 
    return $message;
}


function fd_receipts($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }  

    if ($resp['responseCode'] == 0) {
        $fd = $common->fixed_receipts ($resp['responseData']['token']);
        if ($fd['responseCode'] == 0) {
            if (apc_exists($session_id . "_receipts")) {
                apc_delete($session_id . "_receipts");
            } 
            apc_store($session_id . "_receipts", $fd, 600); 


            $message = "CON Your FD receipts\r\n";
            $reciept = $fd['responseData']['receipts']['receipts'];
            if (count($reciept) > 0) {
                $i = 1; 
                foreach ($reciept as $r) {
                    $message .=  $i . ". " . $r['ReceiptID'] . " - " . $r['ReceiptAmount'] .  "\r\n";
                    $i++;
                }
            } else {
                $message = "CON You currently do not have any FD receipts\r\n\r\n0. Back\r\n00. Home";
            }
        } else {
            $message = $messages['ERROR'];
        }
    } 
    return $message;
}





function confirm_fd_receipts($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $user_details, $session;
    $details = array_reverse(explode("*", $session_string));
    
    if (apc_exists($session_id . "_receipts")) {
        $fd =apc_fetch($session_id . "_receipts");
    } 

    $reciept = $fd['responseData']['receipts']['receipts'];

    if ($details[0] <= count($reciept)) {
        $message = "CON " . sprintf($messages['FDWD_CONFIRM'], $reciept[$details[0] - 1]['ReceiptID']);
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));

        $message = "CON Your FD receipts\r\n";
        $reciept = $fd['responseData']['receipts']['receipts'];
        if (count($reciept) > 0) {
            $i = 1; 
            foreach ($reciept as $r) {
                $message .=  $i . ". " . $r['ReceiptID'] . " - " . $r['ReceiptAmount'] .  "\r\n";
                $i++;
            }
        } else {
            $message = "CON You currently do not have any FD receipts\r\n\r\n0. Back\r\n00. Home";
        }

    }
    return $message; ;
}



function fd_withdraw($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common, $session;
    $details = array_reverse(explode("*", $session_string));
    $pin = $details[0];

    $login = $common->login($msisdn, $pin);
    
    if ($login['responseCode'] == 0) {
        if (apc_exists($session_id . "_receipts")) {
            $fd = apc_fetch($session_id . "_receipts");
        } 

        $reciept = $fd['responseData']['receipts']['receipts'];
        
        

        $resp = $common->fixed_withdrawal ($reciept[$details[1] - 1]['ReceiptID'], "MFANISI", $login['responseData']['token']);
        
        if ($resp['responseCode'] == 0) {
            $message = "CON " . "Processing request. Kindly await approval message\r\n\r\n0. Back\r\n00. Home";
        } else {
            $message = $messages['ERROR'];
        }
    } else {
        $current_menu = explode("*", $current_menu);
        $session_string = explode("*", $session_string);
        array_pop($current_menu);
        array_pop($session_string);
        $session->updateSessions($session_id, $msisdn, implode("*", $current_menu), implode("*", $session_string));
        $message = "CON Invalid PIN. Enter PIN"; 
    }
    return $message;
}


function mini_statement($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $details = array_reverse(explode("*", $session_string));
    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }
    $product = "";
    if ($details[0] == "1") {
        $product = "ts"; 
    } else if ($details[0] == "2") {
        $product = "st";
    }  else  if ($details[0] == "3") {
        $product = "fd";
    }
    $stat = $common->statement($product, $resp['responseData']['token']);
    if ($stat['responseCode'] == 0) {
        $statements = $stat['responseData']['statements'] ;
        if (count($statements) > 0) {

            $message = "CON Statement\n";
             
            foreach ($statements as $st) {
                $message .=  'DR:' . $st['Debit'] . ":CR:" . $st['Credit'] . "\n";
            }
        } else {
	    $message = "END NO transactions to display"; 
        }
    }
    return $message;
}


function check_balance($menu, $msisdn, $session_id, $current_menu, $session_string, $status) {
    global $messages, $common;
    $details = array_reverse(explode("*", $session_string));

    if (apc_exists($session_id)) {
        $resp = apc_fetch($session_id);
    }

    if ($details[0] == "1") {
        $bal = $common->check_balance ($resp['responseData']['token']);
        
        if ($bal['responseCode'] == 0) {
            $message =  "CON " . sprintf($messages['BALANCE'], "M-Fanisi Account", $bal['responseData']['balance']);
        } else {
            $message = $messages['ERROR'];
        }
    } else  if ($details[0] == "2") {
        $lb = $common->loan_balance($resp['responseData']['token']);
        

        if ($lb['responseCode'] == 0) {
            $message = "CON " . sprintf($messages['LOAN_BALANCE'], $lb['responseData']['balance']);
        } else {
            $message = $messages['ERROR'];
        }
    } else  if ($details[0] == "3") {
        $bal = $common->savings_balance ($resp['responseData']['token']);
        if ($bal['responseCode'] == 0) {
            $message = "CON " . sprintf($messages['BALANCE'], "Saving Account",  $bal['responseData']['balance']);
        } else {
            $message = $messages['ERROR'];
        }
    } else  if ($details[0] == "4") {
        $fd = $common->fixed_receipts ($resp['responseData']['token']);
        if ($fd['responseCode'] == 0) {
            $message = sprintf("CON Dear Customer, your Fixed Deposit Account Balance is Ksh. %s\r\nYour FD receipts\r\n", $fd['responseData']['receipts']['balance']);
            $reciept = $fd['responseData']['receipts']['receipts'];
            if (count($reciept) > 0) {
                $i = 1; 
                foreach ($reciept as $r) {
                    $message .=  $i . ". " . $r['ReceiptID'] . " - " . $r['ReceiptAmount'] .  "\r\n";
                    $i++;
                }
            } else {
                $message = "CON You currently do not have any FD receipts\r\n\r\n0. Back\r\n00. Home";
            }
        } else {
            $message = $messages['ERROR'];
        }
    }    
    return $message;
}
$msisdn = isset($_REQUEST['MSISDN']) ? $_REQUEST['MSISDN'] : "";
$sessionId = isset($_REQUEST['session_id']) ? $_REQUEST['session_id'] : "";
$ussdString = isset($_REQUEST['ussd_string']) ? $_REQUEST['ussd_string'] : "";


error_log(date("Y-m-d H:i:s").":[MSISDN] $ussdString [USSD_STING] $ussdString [SESSION_ID] $sessionId [RESPONSE] $ussdString\n", 3, LOG_FILE);
echo $response;
